import './style.css';
import './app.css';

import logo from './assets/images/logo-universal.png';
import {Greet, GreetPerson} from '../wailsjs/go/main/App';
import { main } from "../wailsjs/go/models";

document.querySelector('#app').innerHTML = `
    <img id="logo" class="logo">
      <div class="result" id="result">Please enter your name below FRANK 👇</div>
      <div class="input-box" id="input">
        <input class="input" id="name" type="text" autocomplete="off" />
        <button class="btn" onclick="greet()">Greet</button>
      </div>
    </div>
`;
document.getElementById('logo').src = logo;

let nameElement = document.getElementById("name");
nameElement.focus();
let resultElement = document.getElementById("result");

// Setup the greet function
window.greet = function () {
    // Get name
    let name = nameElement.value;

    // Check if the input is empty
    if (name === "") return;

    // Call App.Greet(name)
    try {
        GreetPerson({_name: name})
            .then((result) => {
                // Update result with data back from App.Greet()
                console.log(result)
                // resultElement.innerText = result;
            })
            // .catch((err) => {
            //     console.error(err);
            // });
    } catch (err) {
        console.error(err);
    }
};
