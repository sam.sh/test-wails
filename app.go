package main

import (
	"context"
)

// App struct
type App struct {
	ctx context.Context
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
}

type Person struct {
	Name    string  `json:"_name"`
	Age     uint8   `json:"age"`
	Address Address `json:"address"`
}

type Address struct {
	Street   string `json:"street"`
	Postcode string `json:"postcode"`
}

func (a *App) Greet(name string) (Person, error) {
	return Person{name, 0, Address{"", ""}}, nil
}

func (a *App) GreetPerson(p Person) (Person, error) {
	return p, nil
}
